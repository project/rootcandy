<?php

function _rootcandy_admin_links() {
  global $user;
  if ($user->uid) {
    $links[] = '<a href="'. url('user') .'" class="user-name">'. $user->name .'</a>';
    $links[] = '<a href="'. url('logout') .'">Logout</a>';
    $links = implode(' | ', $links);
  }

  return $links;
}

function rootcandy_body_class($left = NULL, $right = NULL) {
  $class = '';
  if ($left != '' AND $right) {
    $class = 'sidebars';
  }
  else if ($left != '' AND $right == '') {
    $class = 'sidebar-left';
  }
  else if ($left == '' AND $right != '') {
    $class = 'sidebar-right';
  }

  // Add classes describing the menu trail of the page.
  $class = rootcandy_get_page_classes();

  if (isset($class)) {
    print ' class="'. $class .'"';
  }

}

function _rootcandy_admin_navigation() {
  $path = base_path() . path_to_theme();
  $base = path_to_theme();


  /**
   *
   */
  // get users role
  global $user;
  if ($user->uid) {
    $role = end(array_keys($user->roles));
    $rootcandy_navigation = theme_get_setting('rootcandy_navigation_source_'.$role);
  }
  else {
    $rootcandy_navigation = theme_get_setting('rootcandy_navigation_source_1');
  }

  if ($rootcandy_navigation) {
    $menu_tree = menu_navigation_links($rootcandy_navigation);
  }
  else {
    // build default menu
    $menu_tree[] = array('href' => 'admin', 'title' => 'Dashboard');
    $menu_tree[] = array('href' => 'admin/content', 'title' => 'Content');
    if (variable_get('node_admin_theme', '0')) {
      $menu_tree[] = array('href' => 'node/add', 'title' => 'Create content');
    }
    $menu_tree[] = array('href' => 'admin/build', 'title' => 'Building');
    $menu_tree[] = array('href' => 'admin/settings', 'title' => 'Configuration');
    $menu_tree[] = array('href' => 'admin/user', 'title' => 'Users');
    $menu_tree[] = array('href' => 'admin/reports', 'title' => 'Reports');
    $menu_tree[] = array('href' => 'admin/help', 'title' => 'Help');
  }

  if ($menu_tree) {
    $output = '<ul>';
    foreach ($menu_tree as $item) {
      $arg = array();
      $arg = explode('/', $item['href']);
      if ((arg(0) == $arg[0] AND isset($arg[1]) && arg(1) == $arg[1]) || (arg(0) == $arg[0] AND !arg(1) AND !$arg[1])) {
        $id = ' id="current"';
      }
      else {
        $id = '';
      }
      // icons
      if (!theme_get_setting('rootcandy_navigation_icons')) {
        $iurl = implode("-", $arg);
        $size = theme_get_setting('rootcandy_navigation_icons_size');
        if (!isset($size)) $size = 24;
        $icon = _rootcandy_icon($iurl, $size, 'admin');
        if ($icon) $icon = $icon .'<br />';
      }
      $output .= '<li'. $id .'><a href="'. url($item['href']) .'">'. $icon . $item['title'] .'</a>';
      $output .= '</li>';
    }
    $output .= '</ul>';
  }

  return $output;
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function rootcandy_preprocess_page(&$vars) {
  // get secondary links
  $vars['tabs2'] = menu_secondary_local_tasks();

  // color.module integration
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  if (arg(0) == 'admin' || (variable_get('test',1) AND ((arg(0) == 'node' AND is_numeric(arg(1)) AND arg(2) == 'edit') || (arg(0) == 'node' AND arg(1) == 'add')))) {
    $vars['go_home'] = '<a href="'.base_path().'">'.t('Go Back to Homepage').'</a>';
  }

  // get theme settings
  $vars['hide_header'] = theme_get_setting('rootcandy_header_display');

  // append legal notice
  $vars['closure'] .= '<div id="legal-notice">Theme created by <a href="http://sotak.co.uk" target="_blank">Marek Sotak</a></div>';
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function rootcandy_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function _rootcandy_icon($name, $size = '16', $subdir = '') {
  $path = path_to_theme();
  if ($subdir) {
    $subdir = $subdir .'/';
  }
  $icon = $path .'/icons/i'. $size .'/'. $subdir . $name .'.png';
  return theme('image', $icon);
}


/**
 * Read the theme settings' default values from the .info and save them into the database.
 *
 * @param $theme
 *   The actual name of theme that is being checked.
 */
function rootcandy_settings_init($theme) {
  $themes = list_themes();

  // Get the default values from the .info file.
  $defaults = $themes[$theme]->info['settings'];

  // Get the theme settings saved in the database.
  $settings = theme_get_settings($theme);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_'. $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

/*
 * In addition to initializing the theme settings during HOOK_theme(), init them
 * when viewing/resetting the admin/build/themes/settings/THEME forms.
 */
if (arg(0) == 'admin' && arg(2) == 'themes' && arg(4)) {
  global $theme_key;
  rootcandy_settings_init($theme_key);
}

function rootcandy_get_page_classes($path = NULL) {
  if (!isset($path)) $path = $_GET['q'];
  if ($path) {
    $classes = '';
    $menu_item = explode('/', $path);
    if (count($menu_item)) {
      foreach ($menu_item as $key => $page) {
        $menu_item[$key] = strtr($page, '-', '_');
      }

      do {
        $classes .= ' '. implode('-', $menu_item);
        array_pop($menu_item);
      } while (count($menu_item));
    }
  }

  return $classes;
}

function rootcandy_breadcrumb($breadcrumb) {
  global $language;
  if (!empty($breadcrumb)) {
    if ($language->direction) {
      return '<div class="breadcrumb">'. implode(' « ', array_reverse($breadcrumb)) .'</div>';
    }
    else {
      return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
    }
  }
}
